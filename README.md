[![State-of-the-art Shitcode](https://img.shields.io/static/v1?style=for-the-badge&label=State-of-the-art&message=Shitcode&color=7B5804)](https://github.com/trekhleb/state-of-the-art-shitcode)
[![PipleStatus](https://img.shields.io/gitlab/pipeline/serverfrog/kashboard/master?style=for-the-badge)](https://gitlab.com/Serverfrog/kashboard/pipelines/master/latest)
[![awesomeKotlin](https://img.shields.io/badge/awesome-Kotlin-00a9ff?style=for-the-badge)](https://github.com/KotlinBy/awesome-kotlin)
[![codecov](https://codecov.io/gl/Serverfrog/kashboard/branch/master/graphs/icicle.svg?token=Je52DBRBS3)](https://codecov.io/gl/Serverfrog/kashboard)


# Kashboard



<!-- TABLE OF CONTENTS -->
## Table of Contents

* [About the Project](#about-the-project)
  * [Built With](#built-with)
* [Getting Started](#getting-started)
  * [Prerequisites](#prerequisites)
  * [Installation](#installation)
* [Usage](#usage)
* [Roadmap](#roadmap)
* [Contributing](#contributing)
* [License](#license)
* [Contact](#contact)
* [Acknowledgements](#acknowledgements)



<!-- ABOUT THE PROJECT -->
## About The Project

[![Product Name Screen Shot][product-screenshot]](https://example.com)

There are not so many Dashboards that can be used as XFD on a TV for Developers. So i created a own one

Here's why:
* Dashboards on TV's can Improve the way we work together.
* Build Status, Pending Code Reviews, Code Quality Checks,....
* Plugins to Develop new Widgets 



Of course, no one Dashboard will serve all projects since your needs may be different. So I'll be adding more in the near future. You may also suggest changes by forking this repo and creating a pull request or opening an issue.

A list of commonly used resources that I find helpful are listed in the acknowledgements.

### Built With
* [Kotlin](https://kotlinlang.org/)
* [kotlinx.html](https://github.com/Kotlin/kotlinx.html)
* [materializecss](https://materializecss.com/)



<!-- GETTING STARTED -->
## Getting Started

This is an example of how you may give instructions on setting up your project locally.
To get a local copy up and running follow these simple example steps.

### Prerequisites

* JDK 11 (Tested wit Amazon Correto)
* Gradle 6.0.1

### Installation

1. Clone the repo
```sh
git clone https://gitlab.com/Serverfrog/kashboard.git
```
3. Run Gradle
```sh
gradle assemble
```


<!-- USAGE EXAMPLES -->
## Usage

Use this space to show useful examples of how a project can be used. Additional screenshots, code examples and demos work well in this space. You may also link to more resources.

_For more examples, please refer to the [Documentation](https://gitlab.com/Serverfrog/kashboard/-/wikis/home)_



<!-- ROADMAP -->
## Roadmap

See the [open issues](https://gitlab.com/Serverfrog/kashboard/issues) for a list of proposed features (and known issues).



<!-- CONTRIBUTING -->
## Contributing

Contributions are what make the open source community such an amazing place to be learn, inspire, and create. Any contributions you make are **extremely appreciated**.

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Pull Request



<!-- LICENSE -->
## License

Distributed under the GPL v3 License. See `LICENSE` for more information.



<!-- CONTACT -->
## Contact

Project Link: [https://gitlab.com/Serverfrog/kashboard.git](https://gitlab.com/Serverfrog/kashboard.git)
