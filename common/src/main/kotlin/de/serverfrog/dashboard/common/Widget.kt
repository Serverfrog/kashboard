package de.serverfrog.dashboard.common

import kotlinx.serialization.Serializable

@Serializable
data class Widget(
        var name: String,
        var height: Int,
        var width: Int,
        var x: Int,
        var y: Int,
        var config: String
) {

}
