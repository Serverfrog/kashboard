package de.serverfrog.dashboard.common
import kotlinx.serialization.Serializable

@Serializable
data class Dashboard(
        var name: String,
        var widgets: List<Widget>,
        var config: List<Config>
) {
}