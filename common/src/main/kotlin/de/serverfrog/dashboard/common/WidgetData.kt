package de.serverfrog.dashboard.common

import kotlinx.serialization.Serializable

@Serializable
data class WidgetData(
        var widget: Widget,
        var data: String
) {
}