package de.serverfrog.dashboard.common
import kotlinx.serialization.*

@Serializable
data class Config(
         var name: String,
         var config: Map<String, String>
) {
}
