package de.serverfrog.dashboard.server

import io.micronaut.http.MediaType
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get
import io.micronaut.http.annotation.PathVariable
import javax.inject.Inject

import de.serverfrog.dashboard.common.Dashboard

@Controller("/dashboards")
class DashboardService {

    @Inject
    lateinit var dashboardProvider: DashboardProvider

    @Get("/{name}", produces = [MediaType.APPLICATION_JSON])
    fun getDashboard(@PathVariable name: String): Dashboard? {
        dashboardProvider.updateDashboards()

        return dashboardProvider.dashboards[name]
    }
    @Get(produces = [MediaType.APPLICATION_JSON])
    fun index(): MutableSet<String> {
        dashboardProvider.updateDashboards()
        return dashboardProvider.dashboards.keys
    }

}