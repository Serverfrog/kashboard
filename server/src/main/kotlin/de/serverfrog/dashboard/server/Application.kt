package de.serverfrog.dashboard.server

import io.micronaut.runtime.Micronaut

object Application {

    @JvmStatic
    fun main(args: Array<String>) {
        Micronaut.build()
                .packages("de.serverfrog.dashboard")
                .mainClass(Application.javaClass)
                .start()
    }
}