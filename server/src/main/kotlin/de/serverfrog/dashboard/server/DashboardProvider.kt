package de.serverfrog.dashboard.server


import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import io.micronaut.context.annotation.Value
import java.io.File
import java.nio.file.Paths
import javax.inject.Singleton
import de.serverfrog.dashboard.common.Dashboard


@Singleton
class DashboardProvider(

        @Value("\${dashboard.dashboard-location}")
        private val folderPath: String

) {

    private val dashboardFolder = Paths.get(folderPath)

    private val mapper = jacksonObjectMapper()

    val dashboards = mutableMapOf<String, Dashboard>()

    fun updateDashboards() {
        val map = getDashboards().associateBy { it -> it.name }
        dashboards.clear()
        dashboards.putAll(map)
    }

    private fun getDashboards(): List<Dashboard> {
        val listFiles = dashboardFolder.toFile().listFiles()
        println("search in Folder ${dashboardFolder.toAbsolutePath()}")
        return listFiles.map { file -> parseDashboard(file) }
    }

    private fun parseDashboard(file: File): Dashboard {
        return mapper.readValue<Dashboard>(file, Dashboard::class.java)
    }

}