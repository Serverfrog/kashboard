package de.serverfrog.dashboard.server

import de.serverfrog.dashboard.common.Widget
import de.serverfrog.dashboard.common.WidgetData
import de.serverfrog.dashboard.widgets.WidgetLoader
import io.micronaut.http.MediaType
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get
import io.micronaut.http.annotation.PathVariable
import javax.inject.Inject

@Controller("/widget")
class WidgetService {

    @Inject
    lateinit var dashboardProvider: DashboardProvider
    @Inject
    lateinit var widgetLoader: WidgetLoader

    @Get("/{dashboard}/{widgetName}", produces = [MediaType.APPLICATION_JSON])
    fun getData(@PathVariable dashboard: String,
                @PathVariable widgetName: String
    ): WidgetData {
        widgetLoader.loadingWidgets()

        val widget = getWidget(dashboard, widgetName)
        val pluggedInWidget = widgetLoader.getWidget(widget.name)
        val execute = pluggedInWidget.execute(widget)
        println("Got executed Data $execute")
        return WidgetData(widget, execute)
    }

    private fun getWidget(dashboardName: String, widget: String):Widget{
        val dashboard = dashboardProvider.dashboards[dashboardName]!!
        return dashboard.widgets.first { it.name == widget }

    }
}