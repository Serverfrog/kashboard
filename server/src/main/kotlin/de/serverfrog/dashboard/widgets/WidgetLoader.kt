package de.serverfrog.dashboard.widgets

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import de.swirtz.ktsrunner.objectloader.KtsObjectLoader
import io.micronaut.context.annotation.Value
import io.micronaut.discovery.event.ServiceStartedEvent
import io.micronaut.scheduling.annotation.Async
import java.io.File
import java.nio.file.Files
import java.nio.file.Paths
import java.util.concurrent.locks.ReentrantReadWriteLock
import javax.inject.Singleton
import javax.script.ScriptEngineManager
import kotlin.concurrent.write


@Singleton
class WidgetLoader(

        @Value("\${dashboard.widget-location}")
        private val folderPath: String
) {

    private val scriptEngineManager = ScriptEngineManager().getEngineByExtension("kts")

    var rwLock = ReentrantReadWriteLock()

    var pluggedInWidgets = mutableListOf<PluggedInWidget>()

    private val mapper = jacksonObjectMapper()

    @Async
    @Override
    public fun onApplicationEvent(event: ServiceStartedEvent) {
        loadingWidgets()
    }

    public fun loadingWidgets() {
        println("Load Widget from $folderPath")
        val listFiles = Paths.get(folderPath).toFile().listFiles()
        println("Found following Widget Folders:")
        listFiles.forEach { println("    - $it") }
        loadWidgets(listFiles)
        println("Loaded Widgets: $pluggedInWidgets")
    }

    public fun getWidget(name: String): PluggedInWidget {
        return pluggedInWidgets.first { it.widgetPackage.name == name }
    }

    private fun loadWidgets(listFiles: Array<File>) {
        for (file in listFiles) {
            val loadWidget = loadWidget(file)
            if (loadWidget == null) {
                println("Could not Load widget $file")
                continue
            }
            rwLock.write {
                if (!pluggedInWidgets.contains(loadWidget)) {
                    pluggedInWidgets.add(loadWidget)
                }
            }
            println("Widget ${loadWidget.widgetPackage.name} v${loadWidget.widgetPackage.version} loaded!")
        }

        println("----------------------------------------------")
        println("loading Widgets finished")
        println("----------------------------------------------")
    }


    private fun loadWidget(folder: File): PluggedInWidget? {
        println("----------------------------------------------")
        println("Start loading widget \"$folder\"")

        val validateWidgetFolder = validatePath(folder,true)
        if (validateWidgetFolder.isNotEmpty()) {
            println(validateWidgetFolder)
            return null
        }
        val listFiles = folder.listFiles()
        if (listFiles == null || listFiles.isEmpty()) {
            println("Folder $folder is empty")
            return null
        }
        val widgetJson = listFiles.find { it.name == "widget.json" }
        if (widgetJson == null) {
            println("could not find widget.json inside $folder")
        }
        val widgetPackage = mapper.readValue<WidgetPackage>(widgetJson!!)
        println("Found Widget \"${widgetPackage.name}\" in version ${widgetPackage.version}")

        val serverScriptPath = folder.resolve(widgetPackage.serverScript)
        val validateServerScriptPath = validatePath(serverScriptPath)
        if (validateServerScriptPath.isNotEmpty()) {
            println(validateServerScriptPath)
            return null
        }

        val serverScriptReader = Files.newBufferedReader(serverScriptPath.toPath())
        val serverScriptCompiled: WidgetServerScript = KtsObjectLoader().load(serverScriptReader)

        val dashboardScriptPath = folder.resolve(widgetPackage.dashboardScript)
        val validateDashboardScriptPath = validatePath(dashboardScriptPath)
        if (validateDashboardScriptPath.isNotEmpty()) {
            println(validateDashboardScriptPath)
            return null
        }
        val dashboardScriptReader = Files.newBufferedReader(dashboardScriptPath.toPath())

        val dashboardScriptCompiled: WidgetDashboardScript = KtsObjectLoader().load(dashboardScriptReader)


        return PluggedInWidget(widgetPackage, serverScriptCompiled, dashboardScriptCompiled)
    }

    /**
     * @return a String, when invalid, with a message what is wrong with it
     */
    private fun validatePath(file: File, isDirectory:Boolean = false): String {

        if (!file.exists()) {
            return "$file don't exists"
        }
        if (isDirectory && !file.isDirectory) {
            return "$file isn't a Folder"
        }
        if(!isDirectory && !file.isFile){
            return "$file isn't a file"
        }
        if (!file.canRead()) {
            return "Don't have Read Permission on $file"
        }
        return ""
    }
}