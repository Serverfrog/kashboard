package de.serverfrog.dashboard.widgets

import de.serverfrog.dashboard.common.Widget

class PluggedInWidget(
        val widgetPackage: WidgetPackage,
        val widgetServerScript: WidgetServerScript,
        val widgetDashboardScript: WidgetDashboardScript) {

    public fun execute(widget: Widget): String {
        val data = widgetServerScript.getData(widget)
        return widgetDashboardScript.render(data)
    }

    override fun toString(): String {
        return "PluggedInWidget(widgetPackage=$widgetPackage, " +
                "widgetServerScript=$widgetServerScript," +
                " widgetDashboardScript=$widgetDashboardScript)"
    }


}