package de.serverfrog.dashboard.widgets

data class WidgetPackage(
        var name: String,
        var version: String,
        var serverScript: String,
        var dashboardScript: String
) {
}