package de.serverfrog.dashboard.widgets

import de.serverfrog.dashboard.common.WidgetData

interface WidgetDashboardScript {

    fun render(data: WidgetData): String

}