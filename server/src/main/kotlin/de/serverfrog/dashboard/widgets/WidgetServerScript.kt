package de.serverfrog.dashboard.widgets

import de.serverfrog.dashboard.common.Widget
import de.serverfrog.dashboard.common.WidgetData

interface WidgetServerScript {

    fun getData(widget:Widget):WidgetData
}
