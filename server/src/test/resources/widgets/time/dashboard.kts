package widgets.time

import de.serverfrog.dashboard.common.WidgetData
import de.serverfrog.dashboard.widgets.WidgetDashboardScript
import kotlinx.html.div
import kotlinx.html.id
import kotlinx.html.p
import kotlinx.html.stream.createHTML

class TimeWidget : WidgetDashboardScript {
    override fun render(data: WidgetData): String {
        return createHTML().div("timeWidget") {
            id = "contentrow"
            p {
                +data.data
            }
        }
    }
}

TimeWidget()