package de.serverfrog.dashboard.server

import io.kotlintest.shouldNotBe
import io.kotlintest.specs.BehaviorSpec
import io.micronaut.test.annotation.MicronautTest


@MicronautTest
class WidgetServiceTest(
        private val widgetService: WidgetService
) : BehaviorSpec({

    given("Dashboards with exists") {
        widgetService.dashboardProvider.updateDashboards()
        val dashboard = widgetService.dashboardProvider.dashboards.toList()[0].second
        val widget = dashboard.widgets.first()


        `when`("Widget is get") {
            val result = widgetService.getData(dashboard.name, widget.name)
            then("There exists a Widget with Data") {
                result.data shouldNotBe ""
                result.widget shouldNotBe null
            }
        }
    }
})