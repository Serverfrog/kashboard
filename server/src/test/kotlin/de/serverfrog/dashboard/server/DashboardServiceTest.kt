package de.serverfrog.dashboard.server

import io.kotlintest.shouldBe
import io.kotlintest.shouldNotBe
import io.kotlintest.specs.BehaviorSpec
import io.micronaut.test.annotation.MicronautTest


@MicronautTest
class DashboardServiceTest(
        private val dashboardService: DashboardService
) : BehaviorSpec({

    given("directory with Dashboards exists") {
        `when`("Dashboards are get") {
            val result = dashboardService.index()
            then("There exists a Dashboard") {
                result.size shouldBe 1

            }
            val dashboardName: String = result.first()
            println(dashboardName)
            then("Dashboard is Correct Name") {
                dashboardName shouldBe "Test Dashboard"
            }
            val dashboard = dashboardService.getDashboard(dashboardName)
            then("Dashboard could be get with name") {
                dashboard shouldNotBe null
            }
        }
    }
})