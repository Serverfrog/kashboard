package de.serverfrog.dashboard.server

import de.serverfrog.dashboard.common.Dashboard
import io.kotlintest.shouldBe
import io.kotlintest.specs.BehaviorSpec
import io.micronaut.test.annotation.MicronautTest

@MicronautTest
class DashboardProviderTest(
        private val dashboardProvider: DashboardProvider
) : BehaviorSpec({

    given("directory with Dashboards exists") {
        `when`("Dashboards are get") {
            dashboardProvider.updateDashboards()
            val result = dashboardProvider.dashboards
            then("There exists a Dashboard") {
                result.size shouldBe 1

            }
            val dashboard: Dashboard = result["Test Dashboard"]!!

            then("Dashboard Contains Correct Values"){
                dashboard.name shouldBe "Test Dashboard"

                dashboard.widgets.size shouldBe 1
                dashboard.widgets[0].name shouldBe "Time"
                dashboard.widgets[0].height shouldBe 2
                dashboard.widgets[0].width shouldBe 2
                dashboard.widgets[0].config shouldBe "testWidgetConfig"

                dashboard.config.size shouldBe 1
                dashboard.config[0].name shouldBe "testWidgetConfig"
                dashboard.config[0].config.size shouldBe 1
                dashboard.config[0].config.containsKey("url") shouldBe true
                dashboard.config[0].config["url"] shouldBe "localhost"

            }
        }
    }

})