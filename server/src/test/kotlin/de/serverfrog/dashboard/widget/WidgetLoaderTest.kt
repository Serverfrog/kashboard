package de.serverfrog.dashboard.widget

import de.serverfrog.dashboard.widgets.WidgetLoader
import io.kotlintest.specs.BehaviorSpec
import io.micronaut.test.annotation.MicronautTest

@MicronautTest
class WidgetLoaderTest(
        private val widgetLoader: WidgetLoader
) : BehaviorSpec({

    given("directory with widgets exists") {

        `when`("Widgets are loaded") {
            widgetLoader.loadingWidgets()
            then("There exists a Widget") {
            }

        }
    }
})