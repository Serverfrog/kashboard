package widgets.time

import de.serverfrog.dashboard.common.Widget
import de.serverfrog.dashboard.common.WidgetData
import de.serverfrog.dashboard.widgets.WidgetServerScript
import java.util.*

class TimeWidgetServer : WidgetServerScript {
    override fun getData(widget: Widget): WidgetData {
        val data = "${widget.name} at ${Date()}"
        return WidgetData(widget, data)
    }

}

TimeWidgetServer()