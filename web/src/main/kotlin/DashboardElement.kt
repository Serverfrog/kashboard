import de.serverfrog.dashboard.common.Dashboard
import de.serverfrog.dashboard.common.Widget
import de.serverfrog.dashboard.common.WidgetData
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.html.a
import kotlinx.html.div
import kotlinx.html.dom.append
import kotlinx.html.id
import kotlinx.html.js.onClickFunction
import kotlinx.serialization.KSerializer
import kotlinx.serialization.internal.StringSerializer
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonConfiguration
import kotlinx.serialization.list
import org.w3c.dom.HTMLDivElement
import org.w3c.xhr.XMLHttpRequest
import kotlin.browser.document
import kotlin.browser.window

class DashboardElement(private val name: String) {
    private val dashboard: HTMLDivElement = document.getElementById("dashboard") as HTMLDivElement

    private lateinit var dash: Dashboard

    private lateinit var content: HTMLDivElement

    fun start() {
        val url = "dashboards/$name"
        if (name.isBlank()) {
            println("Get list of dashboards")
            getRequest(url, callback = { parseDashboardList(it) })
        } else {
            println("Dashboard is \"$name\"")

            getRequest(url, callback = { parseDashboard(it) })

        }

    }

    fun outerLoop() {
        GlobalScope.launch(Dispatchers.Default) {
            while (true) {
                dashboardLoop()
            }
        }
    }

    suspend fun dashboardLoop() {
        println("running loop")
        dash.widgets.forEach { w -> getWidgetData(w, dash) }
        delay(5000L)

    }

    fun parseDashboard(json: String) {
        val serializer: KSerializer<Dashboard> = Dashboard.serializer()
        buildDashboard(Json(JsonConfiguration.Stable).parse(serializer, json))
        outerLoop()
    }

    fun parseDashboardList(json: String) {
        val serializer: KSerializer<List<String>> = StringSerializer.list
        buildIndexDashboard(Json(JsonConfiguration.Stable).parse(serializer, json))
    }

    fun buildIndexDashboard(dash: List<String>) {

        dashboard.innerHTML = ""
        println("Render List of Dashboards: $dash")
        dashboard.append {
            div("row") {
                id = "contentrow"
                for (dashboardName in dash) {
                    div("col s12 m6 l3") {
                        id = "content"
                        a("#") {
                            +dashboardName
                            onClickFunction = {
                                window.location.hash = dashboardName
                                window.location.reload()
                            }
                        }

                    }
                }
            }
        }
    }

    fun buildDashboard(dashB: Dashboard) {
        dash = dashB
        println(dash)

        dash.widgets.forEach { w -> getWidgetData(w, dash) }


        dashboard.innerHTML = ""

        dashboard.append {
            div("navbar") {
                id = "navbar"
            }
            div("row") {
                id = "contentrow"
                div("col s12 m3 l2") {
                    id = "sidebar"
                }
                div("col s12 m8 l9") {
                    id = "content"
                }
            }
        }
        content = document.getElementById("content") as HTMLDivElement
    }

    fun getWidgetData(widget: Widget, dash: Dashboard) {
        val url = "widget/${dash.name}/${widget.name}"
        getRequest(url, callback = { buildWidget(it) })
    }

    fun buildWidget(data: String) {
        val widgetData = convertResponse(data)

        println("widgetdata " + widgetData.data)
        content.innerHTML = widgetData.data

    }

    fun convertResponse(json: String): WidgetData {
        return JSON.parse(json)
    }

    fun getRequest(url: String, callback: (String) -> Unit) {
        val xmlHttp = XMLHttpRequest()
        xmlHttp.open("GET", url)
        xmlHttp.onload = {
            if (xmlHttp.readyState == 4.toShort() && xmlHttp.status == 200.toShort()) {
                callback.invoke(xmlHttp.responseText)
            }
        }
        xmlHttp.send()
    }
}