import kotlin.browser.window

fun main(args: Array<String>) {
    window.onload = {
        onLoad()
    }
    println("Bound OnLoad on Window")

}


fun onLoad() {

    println("Dashboard is Starting")
    val hash = window.location.hash.substring(1)

    val dashboard = DashboardElement(hash)
    dashboard.start()

}